# amor-perruno-android
Este proyecto en **KOTLIN** permite registrar y hacer login, para después poder agregar la mascota.
- Se ha utilizado la arquitectura **MVVM**.
- Se ha decidido usar el framework **KOIN** para la inyección de dependencias.
- Se consumen servicios con **RETROFIT**, algunos de manera paralela usando async en el **VIEWMODEL**.
- Se ha utilizado **ROOM** para almacenar las razas de los perros.
- Para la parte del layout, se está utilizando **NAVIGATION** de Jetpack
- Para consumir servicios en un hilo secundario, se esta utilizando **COROUTINES**
- También se puede iniciar sesión con **GMAIL**
- Para optimizar las restauraciones de pantallas, se están utilizando muchas clases abstractas **Base**

Por ahora es un resumen de lo nuevo que hay en kotlin, y la forma q ayudará a recibir las peticiones eficientemente.
